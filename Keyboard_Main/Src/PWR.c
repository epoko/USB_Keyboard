#include "main.h"
#include "KEY_scan.h"
#include "PWR.h"
#include "24l01.h"
#include "spi.h"

void Enter_STOP()
{
	KEY_IO_LOW();
	LED1(0);
	LED2(0);
	LED3(0);
	
	NRF24L01_POWER_Mode(0);

  HAL_PWREx_EnableUltraLowPower();
  HAL_PWREx_EnableFastWakeUp();
  __HAL_RCC_WAKEUPSTOP_CLK_CONFIG(RCC_STOP_WAKEUPCLOCK_MSI);
	
	HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);

// 	HAL_PWR_EnterSTANDBYMode();
	
	KEY_IO_Init();
	key_time_check=HAL_GetTick()+10000;
}


