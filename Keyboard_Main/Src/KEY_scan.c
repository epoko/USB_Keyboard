#include "KEY_scan.h"
#include "main.h"
#include "PWR.h"
#include "24l01.h"

#define KEY_OUT1_HIGH LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_0)
#define KEY_OUT1_LOW LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_0)
#define KEY_OUT2_HIGH LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_1)
#define KEY_OUT2_LOW LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_1)
#define KEY_OUT3_HIGH LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_2)
#define KEY_OUT3_LOW LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_2)
#define KEY_OUT4_HIGH LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_8)
#define KEY_OUT4_LOW LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_8)
#define KEY_OUT5_HIGH LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_9)
#define KEY_OUT5_LOW LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_9)
#define KEY_OUT6_HIGH LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_10)
#define KEY_OUT6_LOW LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_10)
#define KEY_OUT7_HIGH LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_11)
#define KEY_OUT7_LOW LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_11)
#define KEY_OUT8_HIGH LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_12)
#define KEY_OUT8_LOW LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_12)
#define KEY_OUT9_HIGH LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_13)
#define KEY_OUT9_LOW LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_13)
#define KEY_OUT10_HIGH LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_14)
#define KEY_OUT10_LOW LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_14)
#define KEY_OUT11_HIGH LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_15)
#define KEY_OUT11_LOW LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_15)

#define KEY_IN LL_GPIO_ReadInputPort(GPIOA)

uint16_t Key_buf[11];
uint16_t Key_buf_out[11]={0};

uint8_t Send_buf[132],offset=0;



void delay_scan()
{    
	volatile uint16_t i=2;
	while(i--);
}

uint8_t Get_KEY_value()
{
	uint8_t i,flag=0;
	uint16_t temp;
	
	for(i=0;i<11;i++)
	{
		switch(i)
		{
			case 0:KEY_OUT1_LOW;break;
			case 1:KEY_OUT1_HIGH;KEY_OUT2_LOW;break;
			case 2:KEY_OUT2_HIGH;KEY_OUT3_LOW;break;
			case 3:KEY_OUT3_HIGH;KEY_OUT4_LOW;break;
			case 4:KEY_OUT4_HIGH;KEY_OUT5_LOW;break;
			case 5:KEY_OUT5_HIGH;KEY_OUT6_LOW;break;
			case 6:KEY_OUT6_HIGH;KEY_OUT7_LOW;break;
			case 7:KEY_OUT7_HIGH;KEY_OUT8_LOW;break;
			case 8:KEY_OUT8_HIGH;KEY_OUT9_LOW;break;
			case 9:KEY_OUT9_HIGH;KEY_OUT10_LOW;break;
			case 10:KEY_OUT10_HIGH;KEY_OUT11_LOW;break;
		}
		delay_scan();

		temp=(~KEY_IN)&0x0FFF;
		
		if(Key_buf[i]!=temp)
		{
			flag++;
			Key_buf[i]=temp;
		}
	}
	KEY_OUT11_HIGH;
	
	return flag;
}

void KEY_handler()
{
	uint8_t flag,i,j,k;
	static uint8_t gflag=0;
	
	if(Get_KEY_value())
	{
		gflag=1;
	}
	else if(gflag)
	{
		gflag=0;
		
		for(i=0;i<offset;i++)
			Send_buf[i]=0;
		offset=0;
		
		for(i=0;i<11;i++)		//行循环
		{
			for(j=0;j<12;j++)		//列循环
			{
				if(Key_buf[i]&(1<<j))		//有值
				{
					if(Key_buf_out[i]&(1<<j))		//也有值
					{
						Send_buf[offset++]=i*12+j+1;
					}
					else	//无->有
					{
						flag=0;
						for(k=0;k<11;k++)
						{
							if((Key_buf[k]&(1<<j)) && (k!=i))	//除此行还有其他行占用此列
							{
								flag=1;
							}
						}
						
						if((Key_buf[i]^(1<<j)) && (flag==1))	//除此键还有其他键占用此行
						{
						}
						else
						{
							Key_buf_out[i]|=(1<<j);
							Send_buf[offset++]=i*12+j+1;
						}
					}
				}
				else
				{
					Key_buf_out[i]&=~(1<<j);
				}
			}
		}
		
		key_time_check=HAL_GetTick()+10000;
		key_time_last=HAL_GetTick()+1000;
		NRF24L01_TxPacket(Send_buf);
	//	Enter_STOP();
	}
}

void KEY_Send()
{
	NRF24L01_TxPacket(Send_buf);
}

void KEY_IO_LOW()
{
  GPIO_InitTypeDef GPIO_InitStruct;
	
	LL_GPIO_ResetOutputPin(GPIOB, 0xFF07);
	/*
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3 
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7 
                          |GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	*/
	__HAL_GPIO_EXTI_CLEAR_IT(0X0FFF);
	
	HAL_NVIC_EnableIRQ(EXTI0_1_IRQn);
	HAL_NVIC_EnableIRQ(EXTI2_3_IRQn);
	HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);
}

void KEY_IO_Init()
{
	HAL_NVIC_DisableIRQ(EXTI0_1_IRQn);
	HAL_NVIC_DisableIRQ(EXTI2_3_IRQn);
	HAL_NVIC_DisableIRQ(EXTI4_15_IRQn);
	/*
  GPIO_InitTypeDef GPIO_InitStruct;
	
	GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3 
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_6|GPIO_PIN_7 
                          |GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11 
                          |NRF_IRQ_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	*/
	LL_GPIO_SetOutputPin(GPIOB, 0xFF07);
}


