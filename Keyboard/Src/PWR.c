#include "main.h"
#include "KEY_scan.h"
#include "PWR.h"
#include "24l01.h"


void Enter_STOP()
{
	KEY_IO_LOW();
	LED=0;
	
	NRF24L01_POWER_Mode(0);
	
	
	HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);

// 	HAL_PWR_EnterSTANDBYMode();
	
	KEY_IO_Init();
	key_time_check=HAL_GetTick()+10000;
}


