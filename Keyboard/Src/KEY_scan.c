#include "KEY_scan.h"
#include "main.h"
#include "PWR.h"
#include "24l01.h"

#define KEY_OUT1 PAout(0)
#define KEY_OUT2 PAout(1)
#define KEY_OUT3 PAout(2)
#define KEY_OUT4 PAout(3)
#define KEY_OUT5 PAout(4)
#define KEY_OUT6 PAout(5)
#define KEY_OUT7 PAout(6)
#define KEY_OUT8 PAout(7)
#define KEY_OUT9 PBout(0)
#define KEY_OUT10 PBout(1)
#define KEY_OUT11 PCout(15)

#define KEY_IN1 PBin(2)

uint16_t Key_buf[11];
uint16_t Key_buf_out[11]={0};

uint8_t Send_buf[132],offset=0;



void delay_scan()
{    
	volatile uint16_t i=2;
	while(i--);
}

uint8_t Get_KEY_value()
{
	uint8_t i,flag=0;
	uint16_t temp;
	
	for(i=0;i<11;i++)
	{
		switch(i)
		{
			case 0:KEY_OUT1=0;break;
			case 1:KEY_OUT1=1;KEY_OUT2=0;break;
			case 2:KEY_OUT2=1;KEY_OUT3=0;break;
			case 3:KEY_OUT3=1;KEY_OUT4=0;break;
			case 4:KEY_OUT4=1;KEY_OUT5=0;break;
			case 5:KEY_OUT5=1;KEY_OUT6=0;break;
			case 6:KEY_OUT6=1;KEY_OUT7=0;break;
			case 7:KEY_OUT7=1;KEY_OUT8=0;break;
			case 8:KEY_OUT8=1;KEY_OUT9=0;break;
			case 9:KEY_OUT9=1;KEY_OUT10=0;break;
			case 10:KEY_OUT10=1;KEY_OUT11=0;break;
		}
		delay_scan();
		temp=0;
		temp=KEY_IN1;
		temp|=(GPIOB->IDR&0xFF80)>>6;
		temp|=(GPIOC->IDR&0x6000)>>3;
		temp=(~temp)&0x0FFF;
		if(Key_buf[i]!=temp)
		{
			flag++;
			Key_buf[i]=temp;
		}
	}
	KEY_OUT11=1;
	
	return flag;
}

void KEY_handler()
{
	uint8_t flag,i,j,k;
	static uint8_t gflag=0;
	
	if(Get_KEY_value())
	{
		gflag=1;
	}
	else if(gflag)
	{
		gflag=0;
		
		for(i=0;i<offset;i++)
			Send_buf[i]=0;
		offset=0;
		
		for(i=0;i<11;i++)		//行循环
		{
			for(j=0;j<12;j++)		//列循环
			{
				if(Key_buf[i]&(1<<j))		//有值
				{
					if(Key_buf_out[i]&(1<<j))		//也有值
					{
						Send_buf[offset++]=i*12+j+1;
					}
					else	//无->有
					{
						flag=0;
						for(k=0;k<11;k++)
						{
							if((Key_buf[k]&(1<<j)) && (k!=i))	//除此行还有其他行占用此列
							{
								flag=1;
							}
						}
						
						if((Key_buf[i]^(1<<j)) && (flag==1))	//除此键还有其他键占用此行
						{
						}
						else
						{
							Key_buf_out[i]|=(1<<j);
							Send_buf[offset++]=i*12+j+1;
						}
					}
				}
				else
				{
					Key_buf_out[i]&=~(1<<j);
				}
			}
		}
		
		key_time_check=HAL_GetTick()+10000;
		key_time_last=HAL_GetTick()+1000;
		NRF24L01_TxPacket(Send_buf);
	//	Enter_STOP();
	}
}

void KEY_Send()
{
	NRF24L01_TxPacket(Send_buf);
}

void KEY_IO_LOW()
{
  GPIO_InitTypeDef GPIO_InitStruct;
	
	KEY_OUT1=0;
	KEY_OUT2=0;
	KEY_OUT3=0;
	KEY_OUT4=0;
	KEY_OUT5=0;
	KEY_OUT6=0;
	KEY_OUT7=0;
	KEY_OUT8=0;
	KEY_OUT9=0;
	KEY_OUT10=0;
	KEY_OUT11=0;
	
	
  GPIO_InitStruct.Pin = GPIO_PIN_2
												|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9
									|GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_14;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
}

void KEY_IO_Init()
{
  GPIO_InitTypeDef GPIO_InitStruct;
	
  GPIO_InitStruct.Pin = GPIO_PIN_2
												|GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9
									|GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
  GPIO_InitStruct.Pin = GPIO_PIN_13|GPIO_PIN_14;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	
	KEY_OUT1=1;
	KEY_OUT2=1;
	KEY_OUT3=1;
	KEY_OUT4=1;
	KEY_OUT5=1;
	KEY_OUT6=1;
	KEY_OUT7=1;
	KEY_OUT8=1;
	KEY_OUT9=1;
	KEY_OUT10=1;
	KEY_OUT11=1;
	
}


void EXTI2_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
}

void EXTI9_5_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_7);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_8);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_9);
}

void EXTI15_10_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_10);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_11);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_12);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_14);
  HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_15);
}
