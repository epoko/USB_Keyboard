/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "sys.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define NRF_IRQ_Pin GPIO_PIN_13
#define NRF_IRQ_GPIO_Port GPIOC
#define NRF_IRQ_EXTI_IRQn EXTI15_10_IRQn
#define DS_32kHz_Pin GPIO_PIN_14
#define DS_32kHz_GPIO_Port GPIOC
#define DS_INT_Pin GPIO_PIN_15
#define DS_INT_GPIO_Port GPIOC
#define DS_INT_EXTI_IRQn EXTI15_10_IRQn
#define ADC_BAT_Pin GPIO_PIN_0
#define ADC_BAT_GPIO_Port GPIOC
#define SH_CP_Pin GPIO_PIN_1
#define SH_CP_GPIO_Port GPIOC
#define ST_CP_Pin GPIO_PIN_2
#define ST_CP_GPIO_Port GPIOC
#define DS1_Pin GPIO_PIN_3
#define DS1_GPIO_Port GPIOC
#define KEY1_Pin GPIO_PIN_0
#define KEY1_GPIO_Port GPIOA
#define KEY2_Pin GPIO_PIN_1
#define KEY2_GPIO_Port GPIOA
#define VS_RST_Pin GPIO_PIN_4
#define VS_RST_GPIO_Port GPIOC
#define ESP_GPIO0_Pin GPIO_PIN_5
#define ESP_GPIO0_GPIO_Port GPIOC
#define ESP_RST_Pin GPIO_PIN_0
#define ESP_RST_GPIO_Port GPIOB
#define ESP_CH_Pin GPIO_PIN_1
#define ESP_CH_GPIO_Port GPIOB
#define ESP_GPIO2_Pin GPIO_PIN_2
#define ESP_GPIO2_GPIO_Port GPIOB
#define SPI2_CS_Pin GPIO_PIN_12
#define SPI2_CS_GPIO_Port GPIOB
#define VS_XDCS_Pin GPIO_PIN_6
#define VS_XDCS_GPIO_Port GPIOC
#define VS_DREQ_Pin GPIO_PIN_7
#define VS_DREQ_GPIO_Port GPIOC
#define USB_DM_Pin GPIO_PIN_8
#define USB_DM_GPIO_Port GPIOA
#define SPI3_CS_Pin GPIO_PIN_15
#define SPI3_CS_GPIO_Port GPIOA
#define NRF_CSN_Pin GPIO_PIN_6
#define NRF_CSN_GPIO_Port GPIOB
#define NRF_CE_Pin GPIO_PIN_7
#define NRF_CE_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define USB_DM PAout(8)
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
