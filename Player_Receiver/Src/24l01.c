#include "24l01.h"
#include "spi.h"
#include "HC595.h"

const u8 TX_ADDRESS[TX_ADR_WIDTH]={0x89,0x5a,0x31,0x32,0x33}; //发送地址
const u8 RX_ADDRESS[RX_ADR_WIDTH]={0x89,0x5a,0x31,0x32,0x33};//接受地址
u8 TEST_ADDRESS[TX_ADR_WIDTH]={0x8c,0x5a,0x35,0x32,0x31};//测试地址
uint16_t NRF24L01_TX_start;


u8 Rev_Buff[32]={0};

//SPIx 读写一个字节
//TxData:要写入的字节
//返回值:读取到的字节
u8 SPI2_ReadWriteByte(u8 TxData)
{		
	uint8_t Rxdata;
  HAL_SPI_TransmitReceive(&hspi1,&TxData,&Rxdata,1, 10);       
  return Rxdata;                              
}

//初始化24L01
void NRF24L01_Init(void)
{
	u8 buf[5];
	u8 i;
	
	NRF24L01_CE=0; 			//使能24L01
	NRF24L01_CSN=1;			//SPI片选取消
	
	SPI2_ReadWriteByte(0);
	NRF24L01_Write_Buf(NRF_WRITE_REG+TX_ADDR,TEST_ADDRESS,5);//写入5个字节的地址.	
	NRF24L01_Read_Buf(TX_ADDR,buf,5); //读出写入的地址  
	for(i=0;i<5;i++)
	{
		if(buf[i]!=TEST_ADDRESS[i])
			while(1);//检测24L01错误	
	}	
	
//	NRF24L01_TX_Mode();
	NRF24L01_RX_Mode();
	 		 	 
}
//SPI写寄存器
//reg:指定寄存器地址
//value:写入的值
u8 NRF24L01_Write_Reg(u8 reg,u8 value)
{
	u8 status;	
   	NRF24L01_CSN=0;                 //使能SPI传输
  	status =SPI2_ReadWriteByte(reg);//发送寄存器号 
  	SPI2_ReadWriteByte(value);      //写入寄存器的值
  	NRF24L01_CSN=1;                 //禁止SPI传输	   
  	return(status);       			//返回状态值
}
//读取SPI寄存器值
//reg:要读的寄存器
u8 NRF24L01_Read_Reg(u8 reg)
{
	u8 reg_val;	    
 	NRF24L01_CSN = 0;          //使能SPI传输		
  	SPI2_ReadWriteByte(reg);   //发送寄存器号
  	reg_val=SPI2_ReadWriteByte(0XFF);//读取寄存器内容
  	NRF24L01_CSN = 1;          //禁止SPI传输		    
  	return(reg_val);           //返回状态值
}	
//读取status状态寄存器值
u8 NRF24L01_Read_STATUS()
{
	u8 reg_stat;
	NRF24L01_CSN = 0;          //使能SPI传输		
	reg_stat=SPI2_ReadWriteByte(NOP);
	NRF24L01_CSN = 1;          //禁止SPI传输
	return(reg_stat);           //返回状态值
}
//在指定位置读出指定长度的数据
//reg:寄存器(位置)
//*pBuf:数据指针
//len:数据长度
//返回值,此次读到的状态寄存器值 
u8 NRF24L01_Read_Buf(u8 reg,u8 *pBuf,u8 len)
{
	u8 status,u8_ctr;	       
  	NRF24L01_CSN = 0;           //使能SPI传输
  	status=SPI2_ReadWriteByte(reg);//发送寄存器值(位置),并读取状态值   	   
 	for(u8_ctr=0;u8_ctr<len;u8_ctr++)pBuf[u8_ctr]=SPI2_ReadWriteByte(0XFF);//读出数据
  	NRF24L01_CSN=1;       //关闭SPI传输
  	return status;        //返回读到的状态值
}
//在指定位置写指定长度的数据
//reg:寄存器(位置)
//*pBuf:数据指针
//len:数据长度
//返回值,此次读到的状态寄存器值
u8 NRF24L01_Write_Buf(u8 reg, u8 *pBuf, u8 len)
{
	u8 status,u8_ctr;	    
 	NRF24L01_CSN = 0;          //使能SPI传输
	status = SPI2_ReadWriteByte(reg);//发送寄存器值(位置),并读取状态值
	for(u8_ctr=0; u8_ctr<len; u8_ctr++)
		SPI2_ReadWriteByte(*pBuf++); //写入数据	 
	NRF24L01_CSN = 1;       //关闭SPI传输
	return status;          //返回读到的状态值
}				   
//启动NRF24L01发送一次数据
//txbuf:待发送数据首地址
//返回值:发送完成状况
void NRF24L01_TxPacket(u8 *txbuf)
{
//	u8 sta;
	NRF24L01_CE=0;
	NRF24L01_POWER_Mode(1);
//	NRF24L01_Write_Reg(NRF_WRITE_REG+STATUS,NRF24L01_Read_STATUS()); //清除TX_DS或MAX_RT中断标志
//	NRF24L01_Write_Reg(FLUSH_TX,0xff);//清除TX FIFO寄存器 
  	NRF24L01_Write_Buf(WR_TX_PLOAD,txbuf,TX_PLOAD_WIDTH);//写数据到TX BUF  32个字节
 	NRF24L01_CE=1;//启动发送	  
	NRF24L01_TX_start=1;
	
}
//启动NRF24L01接收一次数据

u8 NRF24L01_RxPacket(u8 *rxbuf)
{
	u8 sta;		    							   
//	SPI2_SetSpeed(SPI_BaudRatePrescaler_8); //spi速度为9Mhz（24L01的最大SPI时钟为10Mhz）   
	sta=NRF24L01_Read_Reg(STATUS);  //读取状态寄存器的值    	 
	NRF24L01_Write_Reg(NRF_WRITE_REG+STATUS,sta); //清除TX_DS或MAX_RT中断标志
	if(sta&RX_OK)//接收到数据
	{
		NRF24L01_Read_Buf(RD_RX_PLOAD,rxbuf,RX_PLOAD_WIDTH);//读取数据
		NRF24L01_Write_Reg(FLUSH_RX,0xff);//清除RX FIFO寄存器 
		return 0; 
	}	   
	return 1;//没收到任何数据
}					    
//该函数初始化NRF24L01到RX模式
//设置RX地址,写RX数据宽度,选择RF频道,波特率和LNA HCURR
//当CE变高后,即进入RX模式,并可以接收数据了		   
void NRF24L01_RX_Mode(void)
{
	NRF24L01_CE=0;	    
		NRF24L01_Write_Reg(NRF_WRITE_REG+STATUS,0xff); //清除TX_DS或MAX_RT中断标志
		NRF24L01_TX_start=0;
		NRF24L01_Write_Reg(FLUSH_RX,0xff);//清除TX FIFO寄存器 

		NRF24L01_Write_Buf(NRF_WRITE_REG+RX_ADDR_P0,(u8*)RX_ADDRESS,RX_ADR_WIDTH);//写RX节点地址
  	NRF24L01_Write_Buf(NRF_WRITE_REG+TX_ADDR,(u8*)TX_ADDRESS,TX_ADR_WIDTH);//写TX节点地址 
			

  	NRF24L01_Write_Reg(NRF_WRITE_REG+EN_AA,0x01);    //使能通道0的自动应答    
  	NRF24L01_Write_Reg(NRF_WRITE_REG+EN_RXADDR,0x01);//使能通道0的接收地址  	 	 
		NRF24L01_Write_Reg(NRF_WRITE_REG+SETUP_AW,0x01); //设置地址宽度
		NRF24L01_Write_Reg(NRF_WRITE_REG+SETUP_RETR,0x00);//关闭自动重发
  	NRF24L01_Write_Reg(NRF_WRITE_REG+RF_CH,109);	     //设置RF通信频率		  
  	NRF24L01_Write_Reg(NRF_WRITE_REG+RX_PW_P0,RX_PLOAD_WIDTH);//选择通道0的有效数据宽度 	    
  	NRF24L01_Write_Reg(NRF_WRITE_REG+RF_SETUP,0x27);//设置TX发射参数,0db增益,2Mbps,低噪声增益开启   
  	NRF24L01_Write_Reg(NRF_WRITE_REG+CONFIG, 0x0f);//配置基本工作模式的参数;PWR_UP,EN_CRC,16BIT_CRC,接收模式 
  NRF24L01_CE = 1; //CE为高,进入接收模式 
}						 
//该函数初始化NRF24L01到TX模式
//设置TX地址,写TX数据宽度,设置RX自动应答的地址,填充TX发送数据,选择RF频道,波特率和LNA HCURR
//PWR_UP,CRC使能
//当CE变高后,即进入RX模式,并可以接收数据了		   
//CE为高大于10us,则启动发送.	 
void NRF24L01_TX_Mode(void)
{														 
	NRF24L01_CE=0;	    
		NRF24L01_Write_Reg(NRF_WRITE_REG+STATUS,NRF24L01_Read_STATUS()); //清除TX_DS或MAX_RT中断标志
		NRF24L01_TX_start=0;
		NRF24L01_Write_Reg(FLUSH_TX,0xff);//清除TX FIFO寄存器 
	
  	NRF24L01_Write_Buf(NRF_WRITE_REG+TX_ADDR,(u8*)TX_ADDRESS,TX_ADR_WIDTH);//写TX节点地址 
  	NRF24L01_Write_Buf(NRF_WRITE_REG+RX_ADDR_P0,(u8*)RX_ADDRESS,RX_ADR_WIDTH); //设置TX节点地址,主要为了使能ACK	  

  	NRF24L01_Write_Reg(NRF_WRITE_REG+EN_AA,0x00);     //关闭自动应答    
  	NRF24L01_Write_Reg(NRF_WRITE_REG+EN_RXADDR,0x00); //关闭通道0的接收地址  
		NRF24L01_Write_Reg(NRF_WRITE_REG+SETUP_AW,0x03); //设置地址宽度
  	NRF24L01_Write_Reg(NRF_WRITE_REG+SETUP_RETR,0x00);//关闭自动重发
  	NRF24L01_Write_Reg(NRF_WRITE_REG+RF_CH,109);       //设置RF通道为119
  	NRF24L01_Write_Reg(NRF_WRITE_REG+RF_SETUP,0x27);  //设置TX发射参数,0db增益,2Mbps,低噪声增益开启   
		NRF24L01_Write_Reg(NRF_WRITE_REG+RX_PW_P0,RX_PLOAD_WIDTH);//选择通道0的有效数据宽度 	    
		NRF24L01_Write_Reg(NRF_WRITE_REG+CONFIG,0x0E);    //配置基本工作模式的参数;PWR_UP,EN_CRC,16BIT_CRC,接收模式,开启所有中断
	NRF24L01_CE=1;//CE为高,10us后启动发送
}

void NRF24L01_POWER_Mode(uint8_t mode)
{
	if(mode)
	{
	//	NRF24L01_CE=1;
		NRF24L01_Write_Reg(NRF_WRITE_REG+CONFIG,0x0E);
	}
	else
	{
		NRF24L01_CE=0;
		NRF24L01_Write_Reg(NRF_WRITE_REG+CONFIG,0x0C);
	}
}


void Send_data(uint8_t *buf);
//IRQ中断函数
void NRF_IRQHandler(void)
{
	u8 sta;
	uint8_t i=0;
	
	if(NRF24L01_IRQ)
		return;
	
	sta=NRF24L01_Read_STATUS();  //读取状态寄存器的值	   
	NRF24L01_Write_Reg(NRF_WRITE_REG+STATUS,sta); //清除TX_DS或MAX_RT中断标志

	
	if(sta&RX_OK)//接收到数据
	{
		NRF24L01_Read_Buf(RD_RX_PLOAD,Rev_Buff,RX_PLOAD_WIDTH);
		NRF24L01_Write_Reg(FLUSH_RX,0xff);
		
		for(i=0;Rev_Buff[i];i++)
		{
			printf("%d ",Rev_Buff[i]);
		}
		if(i)
			printf("\r\n");
		
		Send_data(Rev_Buff);
		
		Display[0]=Rev_Buff[0];
		Display[1]=Rev_Buff[1];
		Display[2]=Rev_Buff[2];
		Display[3]=Rev_Buff[3];
		Display[4]=Rev_Buff[4];
		LED3=LED3?0:1;
	}
	else
	{
		printf("error: 0x%02X\r\n",sta);
	}
	
}


