#include "main.h"
#include "usbd_hid.h"
#include "usb_device.h"

uint8_t Send_buf[32]={0};

void Send_data(uint8_t *buf)
{
	uint8_t i;
	uint8_t offset=2;
	
	for(i=0;i<8;i++)
		Send_buf[i]=0;
	
	for(i=0;buf[i];i++)
	{
		switch(buf[i])
		{
			
			case 0:break;
			case 1:Send_buf[offset++]=98;break;
			case 2:Send_buf[offset++]=9;break;
			case 3:break;
			case 4:Send_buf[offset++]=54;break;
			case 5:Send_buf[0]|=0x10;break;
			case 6:break;
			case 7:Send_buf[offset++]=4;break;
			case 8:break;
			case 9:break;
			case 10:Send_buf[offset++]=13;break;
			case 11:Send_buf[offset++]=55;break;
			case 12:Send_buf[offset++]=75;break;
			case 13:Send_buf[offset++]=72;break;
			case 14:Send_buf[offset++]=16;break;
			case 15:Send_buf[offset++]=87;break;
			case 16:Send_buf[offset++]=7;break;
			case 17:break;
			case 18:Send_buf[0]|=0x40;break;
			case 19:Send_buf[offset++]=74;break;
			case 20:break;
			case 21:break;
			case 22:Send_buf[offset++]=81;break;
			case 23:Send_buf[offset++]=97;break;
			case 24:Send_buf[offset++]=68;break;
			case 25:Send_buf[offset++]=22;break;
			case 26:Send_buf[offset++]=83;break;
			case 27:Send_buf[offset++]=93;break;
			case 28:Send_buf[offset++]=84;break;
			case 29:break;
			case 30:break;
			case 31:Send_buf[offset++]=15;break;
			case 32:Send_buf[0]|=0x20;break;
			case 33:break;
			case 34:Send_buf[offset++]=67;break;
			case 35:Send_buf[offset++]=47;break;
			case 36:Send_buf[offset++]=56;break;
			case 37:Send_buf[offset++]=24;break;
			case 38:Send_buf[offset++]=66;break;
			case 39:Send_buf[offset++]=85;break;
			case 40:Send_buf[offset++]=80;break;
			case 41:break;
			case 42:break;
			case 43:Send_buf[offset++]=36;break;
			case 44:Send_buf[0]|=0x02;break;
			case 45:break;
			case 46:Send_buf[offset++]=57;break;
			case 47:Send_buf[offset++]=63;break;
			case 48:Send_buf[offset++]=5;break;
			case 49:Send_buf[offset++]=70;break;
			case 50:Send_buf[offset++]=79;break;
			case 51:Send_buf[offset++]=42;break;
			case 52:Send_buf[offset++]=45;break;
			case 53:Send_buf[0]|=0x01;break;
			case 54:break;
			case 55:Send_buf[offset++]=18;break;
			case 56:break;
			case 57:break;
			case 58:Send_buf[offset++]=34;break;
			case 59:Send_buf[offset++]=29;break;
			case 60:Send_buf[offset++]=35;break;
			case 61:break;
			case 62:break;
			case 63:Send_buf[offset++]=52;break;
			case 64:Send_buf[offset++]=19;break;
			case 65:break;
			case 66:Send_buf[offset++]=11;break;
			case 67:Send_buf[offset++]=48;break;
			case 68:Send_buf[offset++]=6;break;
			case 69:Send_buf[offset++]=17;break;
			case 70:Send_buf[offset++]=30;break;
			case 71:Send_buf[offset++]=53;break;
			case 72:Send_buf[offset++]=21;break;
			case 73:Send_buf[offset++]=92;break;
			case 74:Send_buf[offset++]=49;break;
			case 75:break;
			case 76:Send_buf[offset++]=14;break;
			case 77:Send_buf[offset++]=32;break;
			case 78:Send_buf[offset++]=20;break;
			case 79:Send_buf[offset++]=46;break;
			case 80:Send_buf[offset++]=31;break;
			case 81:Send_buf[offset++]=61;break;
			case 82:Send_buf[offset++]=43;break;
			case 83:Send_buf[offset++]=27;break;
			case 84:Send_buf[offset++]=60;break;
			case 85:Send_buf[offset++]=78;break;
			case 86:Send_buf[offset++]=37;break;
			case 87:Send_buf[offset++]=40;break;
			case 88:Send_buf[offset++]=86;break;
			case 89:Send_buf[offset++]=62;break;
			case 90:Send_buf[offset++]=101;break;
			case 91:Send_buf[offset++]=96;break;
			case 92:Send_buf[offset++]=95;break;
			case 93:break;
			case 94:Send_buf[offset++]=10;break;
			case 95:Send_buf[offset++]=28;break;
			case 96:Send_buf[offset++]=25;break;
			case 97:break;
			case 98:Send_buf[offset++]=99;break;
			case 99:Send_buf[offset++]=26;break;
			case 100:Send_buf[offset++]=88;break;
			case 101:Send_buf[offset++]=89;break;
			case 102:Send_buf[offset++]=94;break;
			case 103:break;
			case 104:Send_buf[offset++]=69;break;
			case 105:Send_buf[offset++]=8;break;
			case 106:Send_buf[offset++]=12;break;
			case 107:Send_buf[offset++]=65;break;
			case 108:Send_buf[offset++]=82;break;
			case 109:Send_buf[offset++]=71;break;
			case 110:Send_buf[offset++]=73;break;
			case 111:Send_buf[offset++]=90;break;
			case 112:Send_buf[offset++]=64;break;
			case 113:break;
			case 114:break;
			case 115:Send_buf[offset++]=39;break;
			case 116:break;
			case 117:Send_buf[0]|=0x08;break;
			case 118:Send_buf[offset++]=33;break;
			case 119:Send_buf[offset++]=23;break;
			case 120:Send_buf[offset++]=59;break;
			case 121:Send_buf[offset++]=38;break;
			case 122:Send_buf[offset++]=91;break;
			case 123:Send_buf[offset++]=77;break;
			case 124:Send_buf[offset++]=51;break;
			case 125:break;
			case 126:Send_buf[0]|=0x04;break;
			case 127:Send_buf[offset++]=76;break;
			case 128:break;
			case 129:break;
			case 130:Send_buf[offset++]=58;break;
			case 131:Send_buf[offset++]=44;break;
			case 132:Send_buf[offset++]=41;break;

			
		}
		
	}
	
	USBD_HID_SendReport(&hUsbDeviceFS, Send_buf, 8);
}


