#include "HC595.h"

u8 Display[5]={0x3F,0X06,0X5B,0X4F,0X66};
u8 LED1=1,LED2=0,LED3=0;
const u8 Seg[]={0x3F,0X06,0X5B,0X4F,0X66,0X6D,0X7D,0X07,0X7F,0X6F,0X77,0X7C,0X39,0X5E,0X79,0X71,0X80};

const u8 mask_list[]={0x1e,0x1d,0x1b,0x17,0x0f};// (~(1<<nums))&0x1F

void HC595_SET_BYTE(u8 value)
{
	u8 i;
	for(i=0;i<8;i++)
	{
		HC595_DS_OUT=(value&0x80)?1:0;
		HC595_SH_OUT=1;
		value<<=1;
		HC595_SH_OUT=0;
	}
}

void SEG7_Display()
{
	static u8 nums=0,i=0;
	
	i++;
	if(i<4)
		return;
	i=0;
	
	HC595_SET_BYTE((1<<nums)|LED3<<5|LED2<<6|LED1<<7);
//	HC595_SET_BYTE(mask_list[nums]|LED3<<5|LED2<<6|LED1<<7);
	HC595_SET_BYTE(~Display[nums]);
	HC595_ST_OUT=1;
	if(nums>=4)
		nums=0;
	else
		nums++;
	HC595_ST_OUT=0;
}
