# USB Wireless Keyboard

### 介绍
这是一个基于STM32和nRF24L01的无线键盘解决方案，该方案可自定义USB描述符，键位定义，鼠标输入等，拥有很大的扩展性

### 硬件设计
- 无线键盘端采用STM32L051C8T6作为主芯片，休眠功耗<3uA，两节7号电池可待机2年以上，正常使用6个月以上，无线传输延时<3ms
- USB接收端采用STM32F042F6P6作为主芯片，体积引脚最少的stm32方案，最高支持usb2.0全速传输，hid协议最大64kbyte/s

### 软件方案
- 单片机程序使用STM32CubeMx v5.2.0版本生成，可方便实现二次修改移植
- 电路pcb使用立创EDA设计，可方便替换元件，可导出到Altium Designer

### PCB设计
[无线键盘端](https://oshwhub.com/zhangzhi297/keyboard_main)

[USB接收端](https://oshwhub.com/zhangzhi297/keyboard_receiver_copy)

### PCB展示
![输入图片说明](https://images.gitee.com/uploads/images/2020/0915/184748_42e4e196_2046746.png "QQ截图20200915183643.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0915/184757_da24cfdd_2046746.png "QQ截图20200915183741.png")
